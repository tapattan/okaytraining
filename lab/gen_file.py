import random 
import pandas as pd 

# Generate random customer data with extended name variety and "dept support" attribute
def generate_detailed_customer_data(num_records=300):
    first_names = [
        "สมชาย", "สมศรี", "ทวี", "วิไล", "เฉลิม", "ปราณี", "สุชาติ", "พิมพ์", "ประสิทธิ์", "รัชนี",
        "จักรพงษ์", "จารุวรรณ", "วรพล", "สุมาลี", "จันทนา", "สุวรรณ", "พิชัย", "อัญชลี", "กิตติศักดิ์", "ดวงใจ"
    ]
    last_names = [
        "ใจดี", "บ้านสุข", "รักษา", "พึ่งพา", "สุขใจ", "ยิ้มแย้ม", "อารี", "นาคี", "วัฒน์", "อินทร",
        "พงษ์สุข", "ทองแท้", "มั่นคง", "รุ่งเรือง", "ศรีวัฒน์", "พูนสุข", "จิตตกร", "โชคดี", "วราพงศ์", "พัฒนาชัย"
    ]
    genders = ["ชาย", "หญิง"]
    provinces = [
        "กรุงเทพมหานคร", "เชียงใหม่", "ภูเก็ต", "ชลบุรี", "นครราชสีมา", "ขอนแก่น", "สงขลา",
        "สุราษฎร์ธานี", "นครศรีธรรมราช", "ราชบุรี", "อุดรธานี", "เชียงราย", "ลำปาง", "พิษณุโลก",
        "นครสวรรค์", "กาญจนบุรี", "สุโขทัย", "เพชรบุรี", "ระยอง", "ตรัง"
    ]
    credit_types = ["รถยนต์", "รถจักรยานยนต์", "ที่อยู่อาศัย"]
    marital_statuses = ["โสด", "แต่งงาน", "หย่าร้าง"]

    data = []
    for _ in range(num_records):
        first_name = random.choice(first_names)
        last_name = random.choice(last_names)
        gender = random.choice(genders)
        age = random.randint(20, 60)
        salary = random.randint(90, 500) * 100  # Ensures divisibility by 100
        credit_type = random.choice(credit_types)
        province = random.choice(provinces)
        marital_status = random.choice(marital_statuses)
        is_old_customer = random.choice(["yes", "no"])
        npl_history = random.choice(["yes", "no"]) if is_old_customer == "yes" else "no"
        dept_support = random.randint(1, 5)
        
        data.append({
            "ชื่อ": first_name,
            "นามสกุล": last_name,
            "เพศ": gender,
            "อายุ": age,
            "เงินเดือน": salary,
            "ประเภทสินเชื่อ": credit_type,
            "จังหวัด": province,
            "สถานะ": marital_status,
            "ลูกค้าเก่า": is_old_customer,
            "ประวัติ NPL": npl_history,
            "dept support": dept_support
        })
    
    return pd.DataFrame(data)

# Generate data
detailed_customer_data = generate_detailed_customer_data()

# Save to CSV
file_path_detailed = "thai_customer_detailed_data.csv"
detailed_customer_data.to_csv(file_path_detailed, index=False, encoding="utf-8-sig")


