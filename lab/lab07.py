p = {'name':'peter','age':18}
print(len(p)) 
print('----')
print(p['age']) 


k1 = {0:'ant',1:'bird',2:'cat'} 

k2 = ['ant','bird','cat']

print(k1[0], k2[0]) 


item = {} # สร้าง item เป็น dictionary เปล่าๆ 
item['name'] = 'peter' # เพิ่มข้อมูลในช่องที่ชื่อว่า name เก็บค่า peter 
                       # จะมีค่าเท่ากับ list.append

item['address'] = 'ระยอง'
print(item)