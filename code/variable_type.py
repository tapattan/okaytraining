# ประเภทของตัวแปร
p = 10 # int 
p = 12.34 # float
p = 'peter' #str 
p = True # bool
p = False #bool 
p = [1,2,3,4] #list 
p = (10,20,30) #tuple  
p = {'name':'lisa','age':17}  # dictionary 
p = {10,20,30} # set



p = 10 + 5j # complex number
p = 0b111 # เลขฐาน2
p = 0o123 # เลขฐาน8
p = 0xab  # เลขฐาน16

