# สร้าง Dictionary
student_scores = {
    "Alice": 85,
    "Bob": 92,
    "Charlie": 78,
    "David": 88
}

# 1. ใช้ len() เพื่อหาจำนวนสมาชิกใน Dictionary
print(len(student_scores))  # Output: 4

# 2. ใช้ del เพื่อลบสมาชิกออกจาก Dictionary
del student_scores["Charlie"]  # ลบสมาชิกที่มีคีย์เป็น "Charlie"
print(student_scores)  # Output: {'Alice': 85, 'Bob': 92, 'David': 88}

# 3. ใช้ keys() เพื่อดึงคีย์ทั้งหมดออกมา
keys = student_scores.keys()
print(keys)  # Output: dict_keys(['Alice', 'Bob', 'David'])

# 4. ใช้ values() เพื่อดึงค่าทั้งหมดออกมา
values = student_scores.values()
print(values)  # Output: dict_values([85, 92, 88])

# 5. ใช้ clear() เพื่อลบสมาชิกทั้งหมดใน Dictionary
student_scores.clear()
print(student_scores)  # Output: {}

# สร้าง Dictionary ใหม่เพื่อใช้ตัวอย่างต่อไป
student_scores = {
    "Alice": 85,
    "Bob": 92,
    "Charlie": 78,
    "David": 88
}

# 6. ใช้ pop() เพื่ออ่านค่าและลบสมาชิกที่มีคีย์ที่ระบุ
score = student_scores.pop("Bob")
print(score)  # Output: 92
print(student_scores)  # Output: {'Alice': 85, 'Charlie': 78, 'David': 88}

# 7. ใช้ get() เพื่ออ่านค่าของสมาชิกที่มีคีย์ที่ระบุ
alice_score = student_scores.get("Alice")
print(alice_score)  # Output: 85

# 8. ใช้ get() กับคีย์ที่ไม่มีใน Dictionary (จะได้ค่า None)
non_existing_score = student_scores.get("Eve")
print(non_existing_score)  # Output: None