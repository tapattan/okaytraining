# สร้าง List
numbers = [5, 3, 9, 1, 7, 3]
print(numbers)

# 1. ใช้ len() เพื่อหาจำนวนสมาชิกใน List
print(len(numbers))  # Output: 6

# 2. ใช้ max() เพื่อหาสมาชิกที่มีค่ามากที่สุด
print(max(numbers))  # Output: 9

# 3. ใช้ min() เพื่อหาสมาชิกที่มีค่าน้อยที่สุด
print(min(numbers))  # Output: 1

# 4. ใช้ sum() เพื่อหาผลรวมของสมาชิกทั้งหมดใน List
print(sum(numbers))  # Output: 28

# 5. ใช้ del เพื่อลบสมาชิกในตำแหน่งที่กำหนด
del numbers[2]  # ลบสมาชิกที่ตำแหน่งที่ 2 (เลข 9)
print(numbers)  # Output: [5, 3, 1, 7, 3]

# 6. ใช้ append() เพื่อเพิ่มสมาชิกใหม่เข้าไปใน List
numbers.append(10)
print(numbers)  # Output: [5, 3, 1, 7, 3, 10]

# 7. ใช้ clear() เพื่อลบสมาชิกทั้งหมดใน List
numbers.clear()
print(numbers)  # Output: []

# สร้าง List ใหม่เพื่อใช้ตัวอย่างต่อไป
numbers = [5, 3, 9, 1, 7, 3]

# 8. ใช้ count() เพื่อหาจำนวนสมาชิกที่มีค่าเท่ากับค่าที่ระบุ
print(numbers.count(3))  # Output: 2 (เลข 3 ปรากฏ 2 ครั้ง)

# 9. ใช้ pop() เพื่อลบและคืนค่าของสมาชิกในตำแหน่งที่กำหนด
removed_value = numbers.pop(1)  # ลบสมาชิกที่ตำแหน่งที่ 1 (เลข 3)
print(removed_value)  # Output: 3
print(numbers)  # Output: [5, 9, 1, 7, 3]

# 10. ใช้ sort() เพื่อเรียงลำดับสมาชิกใน List จากน้อยไปมาก
numbers.sort()
print(numbers)  # Output: [1, 3, 5, 7, 9]

# 11. ใช้ reverse() เพื่อเรียงลำดับสมาชิกแบบย้อนกลับ
numbers.reverse()
print(numbers)  # Output: [9, 7, 5, 3, 1]