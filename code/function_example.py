# สร้างฟังก์ชันโดยใช้ def
def greet():
    print("Hello, welcome to learning Python functions!")

# เรียกใช้ฟังก์ชัน
greet()



# สร้างฟังก์ชันที่รับชื่อของผู้ใช้
def greet_user(name):
    print(f"Hello, {name}! Nice to meet you.")

# เรียกใช้ฟังก์ชันโดยส่งค่าเข้าไป
greet_user("George")



# สร้างฟังก์ชันที่บวกตัวเลขสองตัว
def add_numbers(a, b):
    return a + b

# เรียกใช้ฟังก์ชัน
result = add_numbers(10, 5)
print(result)  # Output: 15




# สร้างฟังก์ชันที่คืนค่าผลบวกและผลต่างของตัวเลขสองตัว
def calculate(a, b):
    sum_result = a + b
    difference = a - b
    return sum_result, difference

# เรียกใช้ฟังก์ชันและรับค่าที่ส่งคืน
sum_result, difference = calculate(10, 5)
print("ผลบวก:", sum_result)  # Output: ผลบวก: 15
print("ผลต่าง:", difference)  # Output: ผลต่าง: 5



# สร้างฟังก์ชันที่รับ argument จำนวนไม่แน่นอน และคำนวณผลรวมของค่าทั้งหมด
def sum_all(*numbers):
    total = 0
    for number in numbers:
        total += number
    return total

# เรียกใช้ฟังก์ชันโดยส่งค่าเข้าไปหลายตัว
result = sum_all(1, 2, 3, 4, 5)
print("ผลรวม:", result)  # Output: ผลรวม: 15

result = sum_all(10, 20)
print("ผลรวม:", result)  # Output: ผลรวม: 30



def show_info(*args, **kwargs):
    print("Positional arguments:", args)
    print("Keyword arguments:", kwargs)

# เรียกใช้ฟังก์ชันโดยส่งค่าหลายตัว
show_info(1, 2, 3, name="Alice", age=25)
# Output:
# Positional arguments: (1, 2, 3)
# Keyword arguments: {'name': 'Alice', 'age': 25}
