# การ slice ใน string คือการเลือกเฉพาะบางส่วนของ string ออกมา โดยใช้ indexing เพื่อระบุช่วงที่ต้องการ โดยใน Python การ slice string จะใช้รูปแบบ:

#string[start:end:step]
#start: ตำแหน่งเริ่มต้น (รวมตำแหน่งนี้)
#end: ตำแหน่งสิ้นสุด (ไม่รวมตำแหน่งนี้)
#step: ความก้าวหน้าหรือการกระโดดข้าม (เลือกได้ว่าจะก้าวทีละกี่ตัวอักษร)

#ตัวอย่างการใช้ Slice กับ String

text = "Hello, Python!"

#Slice จากตำแหน่งที่ 0 ถึง 4 (ตำแหน่งเริ่มต้นนับจาก 0)
sliced_text = text[0:5]  # เลือกตัวอักษรจากตำแหน่งที่ 0 ถึง 4 (รวมถึงตำแหน่งเริ่มต้น แต่ไม่รวมตำแหน่งสิ้นสุด)
print(sliced_text)  # Output: Hello


#Slice จากตำแหน่งที่ 7 จนถึงจบ string
sliced_text = text[7:]  # เริ่มจากตำแหน่งที่ 7 ไปจนถึงจบ string
print(sliced_text)  # Output: Python!


reversed_text = text[::-1]  # ย้อนกลับ string ทั้งหมด
print(reversed_text)  # Output: !nohtyP ,olleH
#Slice จากตำแหน่ง -6 ถึงตำแหน่ง -1

 