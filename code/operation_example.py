# +,-,*,/,**,//,% 
# : comment 

p = 5
k = 10
ans = p+k  # บวก
print(ans)


p = 5
k = 10
ans = p-k # ลบ
print(ans)


p = 5
k = 10
ans = p*k # คูณ
print(ans)


p = 5
k = 10
ans = p/k  # หาร
print(ans)


p = 5
k = 10
ans = p**k  # ยกกำลัง
print(ans)


p = 12
k = 10
ans = p//k # หารปัดลงเสมอ
print(ans)


p = 12
k = 10
ans = p%k  # หารเอาเศษ
print(ans)

