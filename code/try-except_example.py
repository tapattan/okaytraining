# ตัวอย่างการใช้ try - except - else - finally

try:
    # โค้ดที่อาจเกิดข้อผิดพลาด
    number = int(input("กรุณาใส่ตัวเลขที่ไม่ใช่ศูนย์: "))  # อาจเกิด ValueError ถ้าใส่ข้อมูลที่ไม่ใช่ตัวเลข
    result = 10 / number                                     # อาจเกิด ZeroDivisionError ถ้าใส่เลข 0
except ValueError as e:
    # จัดการกับข้อผิดพลาดที่เกิดจากการแปลงเป็น int ไม่ได้
    print("กรุณาใส่ตัวเลขที่ถูกต้อง (ต้องเป็นตัวเลขเท่านั้น)", e)
except ZeroDivisionError as e:
    # จัดการกับข้อผิดพลาดที่เกิดจากการหารด้วยศูนย์
    print("ไม่สามารถหารด้วยศูนย์ได้:", e)
else:
    # จะทำงานเมื่อไม่มีข้อผิดพลาดเกิดขึ้น
    print("ผลลัพธ์คือ:", result)
finally:
    # จะทำงานเสมอไม่ว่าจะเกิดข้อผิดพลาดหรือไม่
    print("สิ้นสุดการทำงานของโปรแกรม")



print('---------')
print('---------')


def check_age(age):
    if age < 0:
        # สร้างข้อผิดพลาดเองโดยใช้ raise เมื่อค่าอายุเป็นลบ
        raise ValueError("อายุไม่สามารถเป็นค่าลบได้")
    return f"คุณมีอายุ {age} ปี"

try:
    # ทดสอบฟังก์ชันด้วยค่าอายุที่ไม่ถูกต้อง
    age = int(input("กรุณาใส่อายุของคุณ: "))
    result = check_age(age)
    print(result)
except ValueError as e:
    # จัดการกับข้อผิดพลาดที่เกิดจากการใช้ raise
    print("พบข้อผิดพลาด:", e)




print('---------')
print('---------')



def calculate_price(quantity, price_per_item):
    if quantity <= 0:
        raise ValueError("จำนวนสินค้าต้องมากกว่าศูนย์")
    if price_per_item < 0:
        raise ValueError("ราคาต่อหน่วยต้องไม่เป็นค่าลบ")
    return quantity * price_per_item

try:
    qty = int(input("กรุณาใส่จำนวนสินค้า: "))
    price = float(input("กรุณาใส่ราคาต่อหน่วย: "))
    total_price = calculate_price(qty, price)
    print(f"ราคารวมของสินค้าคือ: {total_price:.2f} บาท")
except ValueError as e:
    print("พบข้อผิดพลาด:", e)
