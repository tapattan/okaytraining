# ตัวแปร Global
x = 10

def my_function():
    # ตัวแปร Local
    y = 5
    print("ค่าในฟังก์ชัน (Local y):", y)
    print("ค่าในฟังก์ชัน (Global x):", x)

my_function()  # Output: ค่าในฟังก์ชัน (Local y): 5, ค่าในฟังก์ชัน (Global x): 10

# print(y)  # จะเกิดข้อผิดพลาดเพราะ y เป็นตัวแปร Local ที่ไม่สามารถเข้าถึงได้จากนอกฟังก์ชัน
print("ค่า Global x ภายนอกฟังก์ชัน:", x)  # Output: ค่า Global x ภายนอกฟังก์ชัน: 10




# ตัวแปร Global
x = 20

def modify_global():
    global x  # ระบุว่าเราต้องการใช้ตัวแปร Global ชื่อ x
    x = 50    # เปลี่ยนค่าของตัวแปร Global
    print("ค่าของ x ภายในฟังก์ชันหลังการเปลี่ยนแปลง:", x)

modify_global()  # Output: ค่าของ x ภายในฟังก์ชันหลังการเปลี่ยนแปลง: 50
print("ค่าของ x ภายนอกฟังก์ชันหลังการเปลี่ยนแปลง:", x)  # Output: ค่าของ x ภายนอกฟังก์ชันหลังการเปลี่ยนแปลง: 50




