# สร้างชุดข้อมูล (Set)
set_a = {1, 2, 3, 4, 5}
set_b = {4, 5, 6, 7, 8}

# 1. union() หรือใช้สัญลักษณ์ | เพื่อรวมสมาชิกทั้งหมดจากทั้งสอง set
union_set = set_a.union(set_b)
print(union_set)  # Output: {1, 2, 3, 4, 5, 6, 7, 8}

# หรือใช้ |
union_set = set_a | set_b
print(union_set)  # Output: {1, 2, 3, 4, 5, 6, 7, 8}

# 2. intersection() หรือใช้สัญลักษณ์ & เพื่อหาสมาชิกที่อยู่ในทั้งสอง set
intersection_set = set_a.intersection(set_b)
print(intersection_set)  # Output: {4, 5}

# หรือใช้ &
intersection_set = set_a & set_b
print(intersection_set)  # Output: {4, 5}

# 3. difference() หรือใช้สัญลักษณ์ - เพื่อหาสมาชิกที่อยู่ใน set_a แต่ไม่อยู่ใน set_b
difference_set = set_a.difference(set_b)
print(difference_set)  # Output: {1, 2, 3}

# หรือใช้ -
difference_set = set_a - set_b
print(difference_set)  # Output: {1, 2, 3}

# 4. symmetric_difference() หรือใช้สัญลักษณ์ ^ เพื่อหาสมาชิกที่อยู่ใน set_a หรือ set_b แต่ไม่ใช่ทั้งสอง
symmetric_difference_set = set_a.symmetric_difference(set_b)
print(symmetric_difference_set)  # Output: {1, 2, 3, 6, 7, 8}

# หรือใช้ ^
symmetric_difference_set = set_a ^ set_b
print(symmetric_difference_set)  # Output: {1, 2, 3, 6, 7, 8}