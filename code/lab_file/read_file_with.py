# เปิดไฟล์เพื่ออ่านด้วย with 
# อ่านทีละบรรทัด

# เปิดไฟล์เพื่ออ่าน
with open("example.txt", "r") as file:
    for line in file:
        # อ่านทีละบรรทัด
        print(line.strip())  # ใช้ strip() เพื่อลบ newline (\n) ที่ท้ายบรรทัด
        print('---')