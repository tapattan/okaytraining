# เปิดไฟล์เพื่ออ่าน ('r' = read)
file = open("example.txt", "r")

# อ่านข้อมูลจากไฟล์
content = file.read()
print("เนื้อหาในไฟล์:")
print(content)

 
# ปิดไฟล์เมื่อใช้งานเสร็จ
file.close()
