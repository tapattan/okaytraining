import os

# ระบุโฟลเดอร์ที่มีไฟล์ที่ต้องการอ่าน
folder_path = "content"

# วนลูปอ่านไฟล์ทั้งหมดในโฟลเดอร์
for filename in os.listdir(folder_path):
    # ตรวจสอบว่าเป็นไฟล์ (ไม่ใช่โฟลเดอร์ย่อย)
    if filename.endswith(".txt"):
        file_path = os.path.join(folder_path, filename)
        with open(file_path, "r") as file:
            content = file.read()
            print(f"เนื้อหาในไฟล์ {filename}:")
            print(content)
            print("-" * 20)  # เส้นแบ่งเพื่อความสวยงาม