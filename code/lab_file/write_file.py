# เปิดไฟล์เพื่อเขียน ('w' = write)
file = open("example-write.txt", "w")

# เขียนข้อมูลลงในไฟล์
file.write("นี่คือข้อมูลที่ถูกเขียนลงในไฟล์\n")
file.write("Hello, Python File Management!")

# ปิดไฟล์เมื่อเขียนเสร็จ
file.close()
