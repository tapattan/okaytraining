from my_package import greet_a, greet_b

# เรียกใช้ฟังก์ชัน greet_a และ greet_b ที่ import มาจาก my_package
print(greet_a())  # Output: Hello from Module A
print(greet_b())  # Output: Hello from Module B