from datetime import datetime

# หาวันที่และเวลาปัจจุบัน
current_datetime = datetime.now()
print("วันที่และเวลาปัจจุบัน:", current_datetime)

# แยกเฉพาะวันปัจจุบัน
current_date = current_datetime.date()
print("วันปัจจุบัน:", current_date)

# แยกเฉพาะเวลา
current_time = current_datetime.time()
print("เวลาปัจจุบัน:", current_time)





# การลบกันของเวลา (หา Time Difference)
from datetime import datetime

# กำหนดวันที่และเวลาสองเวลา
start_time = datetime(2024, 11, 20, 14, 30)  # 20 พฤศจิกายน 2024 เวลา 14:30
end_time = datetime(2024, 11, 21, 16, 0)     # 21 พฤศจิกายน 2024 เวลา 16:00

# หาความแตกต่างของเวลา
time_difference = end_time - start_time
print("ความแตกต่างของเวลา:", time_difference)

# แสดงผลออกมาเป็นวันและชั่วโมง
days = time_difference.days
seconds = time_difference.seconds
hours = seconds // 3600
print(f"ความแตกต่าง: {days} วัน และ {hours} ชั่วโมง")  # Output: ความแตกต่าง: 1 วัน และ 1 ชั่วโมง




# การบวกวันและเวลา (Adding Time)
from datetime import datetime, timedelta

# หาวันที่และเวลาปัจจุบัน
current_datetime = datetime.now()
print("วันที่และเวลาปัจจุบัน:", current_datetime)

# บวกจำนวนวัน
days_to_add = 5
new_datetime = current_datetime + timedelta(days=days_to_add)
print(f"วันที่และเวลาหลังจากบวก {days_to_add} วัน:", new_datetime)

# บวกจำนวนชั่วโมง
hours_to_add = 3
new_datetime = current_datetime + timedelta(hours=hours_to_add)
print(f"วันที่และเวลาหลังจากบวก {hours_to_add} ชั่วโมง:", new_datetime)

# บวกจำนวนวันและจำนวนชั่วโมงพร้อมกัน
new_datetime = current_datetime + timedelta(days=2, hours=5)
print("วันที่และเวลาหลังจากบวก 2 วัน และ 5 ชั่วโมง:", new_datetime)
