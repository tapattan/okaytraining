# เครื่องหมายเปรียบเทียบ 
# == , > , >= , < , <= และ != 
# and , or และ not

p = 10
k = 20

if(p < k): 
  print('yes') 


if(p > k):
  print('yes')
else:
  print('no') 



p = 10
k = 10
if(p>k):
  print('yes')
elif(p<k):
  print('no')
else:
  print('n/a')


# กรณีมีหลายๆ เงื่อนไข

a = 10
b = 5 
c = 8
if(a>b and b<c):
  print('yes')


if(a>b or b<c):
  print('yes')


if(not(b > a)):
  print('yes')